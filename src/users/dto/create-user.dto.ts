import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  user_name: string;
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  password: string;
}
