import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

const users: User[] = [
  // {
  //   id: 1,
  //   user_name: 'Adminstrator',
  //   email: 'admin@mail.com',
  //   password: 'Pass@1234',
  //   api_request_quota: 100,
  // },
  // {
  //   id: 2,
  //   user_name: 'User 1',
  //   email: 'user1@mail.com',
  //   password: 'Pass@1234',
  //   api_request_quota: 100,
  // },
  // {
  //   id: 3,
  //   user_name: 'User 2',
  //   email: 'user2@mail.com',
  //   password: 'Pass@1234',
  //   api_request_quota: 100,
  // },
];

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const user_name = createUserDto.user_name;
    const user = await this.usersRepository.findOne({
      where: { user_name },
    });
    if (!!user) {
      throw new ConflictException('User account already exists');
    }
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const refUser = await this.usersRepository.findOneBy({ id });
    if (!refUser) {
      throw new NotFoundException();
    }
    const updatedUser = { ...refUser, ...updateUserDto };
    return this.usersRepository.save(updatedUser);
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
